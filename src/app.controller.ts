import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
} from '@nestjs/common';
import { AppService } from './app.service';

@Controller('')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getDefault(): string {
    return 'Default';
  }

  @Get('hello')
  getHello(): string {
    return '<html><body><h1>Hello Buu!!</h1></body></html>';
  }

  @Patch('world')
  getWorld(): string {
    return 'Hello World';
  }

  @Get('test-query')
  testQuery(
    @Req() req,
    @Query('celsius') celsius: number,
    @Query('type') type: string,
  ) {
    // return { celsius, type };
    return { celsius: celsius, type: type };
  }

  @Get('test-param/:celsius')
  testParam(@Req() req, @Param('celsius') celsius: number) {
    return { celsius };
  }

  @Post('test-body')
  testBody(@Req() req, @Body() body, @Body('celsius') celsius: number) {
    return { celsius };
  }

  // @Get('convert')
  // convert(@Query('celsius') celsius: string) {
  //   // return (celsius * 9.0) / 5 + 32;
  //   return this.appService.convert(parseFloat(celsius));
  // }

  // @Post('convert')
  // convertByPost(@Body('celsius') celsius: number) {
  //   // return (celsius * 9.0) / 5 + 32;
  //   return {
  //     celsius: celsius,
  //     // farenheit: (celsius * 9.0) / 5 + 32,
  //     farenheit: (celsius * 9.0) / 5 + 32,
  //   };
  // }

  // @Get('convert/:celsius')
  // convertByParam(@Param('celsius') celsius: string) {
  //   // return (celsius * 9.0) / 5 + 32;
  //   return this.appService.convert(parseFloat(celsius));
  // }
}
